#include <string>
#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
//#include <tf/transform_broadcaster.h>

// Messages
#include <poppy_upc_description/Full_feedback.h>



float p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12;
float position[12];


void callback_feedback(const poppy_upc_description::Full_feedback::ConstPtr& msg)
{
//   // ROS_INFO("ID of the motor you want to command is %i",msg->id);
//   // ROS_INFO("Goal Position is %i",msg->goalPosition);
//   // ROS_INFO("Goal Velocity is %i",msg->goalSpeed);
//   // com.goal_velocities[msg->id] = msg->goalSpeed;
//   // com.goal_positions[msg->id] = msg->goalPosition;
//     // pos_reference = msg->goal_position;
//     // vel_reference = msg->goal_velocity;
    for (uint8_t i=0; i<12; i++) {
        position[i] = msg->position[i];
        // ROS_INFO("")
    }
}



int main(int argc, char** argv) {
    ros::init(argc, argv, "read_motors_publisher");
    ros::NodeHandle n;
    ros::Publisher joint_pub = n.advertise<sensor_msgs::JointState>("joint_states", 1);


    /* ---------------------- Subscribers ---------------------- */
    ros::Subscriber feedback = n.subscribe<poppy_upc_description::Full_feedback>("/motor_driver/motor_feedback", 1000, &callback_feedback);


//    tf::TransformBroadcaster broadcaster;
    ros::Rate loop_rate(30);

    const double degree = M_PI/180;

    double wise = 1;
    double diferential = 0.007854;

    // robot state
    double base_to_hip_right_1= 0, hip_right_1_to_hip_right_2= 0, hip_right_2_to_thigh_right= 0, thigh_right_to_leg_right= 0, leg_right_to_ankle_right= 0, ankle_right_to_foot_right= 0;
    double base_to_hip_left_1= 0, hip_left_1_to_hip_left_2= 0, hip_left_2_to_thigh_left= 0, thigh_left_to_leg_left= 0, leg_left_to_ankle_left= 0, ankle_left_to_foot_left= 0;

    // message declarations
    sensor_msgs::JointState joint_state;

    while (ros::ok()) {
        //update joint_state
        joint_state.header.stamp = ros::Time::now();
        joint_state.name.resize(12);
        joint_state.position.resize(12);

        joint_state.name[0] ="base_to_hip_right_1";
        joint_state.position[0] = position[0];
        joint_state.name[1] ="hip_right_1_to_hip_right_2";
        joint_state.position[1] = position[1];
        joint_state.name[2] ="hip_right_2_to_thigh_right";
        joint_state.position[2] = position[2];
        joint_state.name[3] ="thigh_right_to_leg_right";
        joint_state.position[3] = position[3];
	joint_state.name[4] ="leg_right_to_ankle_right";
        joint_state.position[4] = position[4];
	joint_state.name[5] ="ankle_right_to_foot_right";
        joint_state.position[5] = position[5];

        joint_state.name[6] ="base_to_hip_left_1";
        joint_state.position[6] = position[6];
        joint_state.name[7] ="hip_left_1_to_hip_left_2";
        joint_state.position[7] = position[7];
        joint_state.name[8] ="hip_left_2_to_thigh_left";
        joint_state.position[8] = position[8];
        joint_state.name[9] ="thigh_left_to_leg_left";
        joint_state.position[9] = position[9];
        joint_state.name[10] ="leg_left_to_ankle_left";
        joint_state.position[10] = position[10];
        joint_state.name[11] ="ankle_left_to_foot_left";
        joint_state.position[11] = position[11];

        //send the joint state and transform
        joint_pub.publish(joint_state);



 //        // Code that mave robot "move"
	// if (hip_right_2_to_thigh_right >= 3.1416){
	// 	wise = -1;}
 //        if (hip_right_2_to_thigh_right <= 0){
 //                wise = 1;}
 //        hip_right_2_to_thigh_right = hip_right_2_to_thigh_right + wise*diferential;

        // This will adjust as needed per iteration
        ros::spinOnce();
        loop_rate.sleep();
    }


    return 0;
}
