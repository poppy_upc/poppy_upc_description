#include <string>
#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
//#include <tf/transform_broadcaster.h>

int main(int argc, char** argv) {
    ros::init(argc, argv, "state_poppy_publisher");
    ros::NodeHandle n;
    ros::Publisher joint_pub = n.advertise<sensor_msgs::JointState>("joint_states", 1);
//    tf::TransformBroadcaster broadcaster;
    ros::Rate loop_rate(30);

    const double degree = M_PI/180;

    double wise = 1;
    double diferential = 0.007854;

    // robot state
    double base_to_hip_right_1= 0, hip_right_1_to_hip_right_2= 0, hip_right_2_to_thigh_right= 0, thigh_right_to_leg_right= 0, leg_right_to_ankle_right= 0, ankle_right_to_foot_right= 0;
    double base_to_hip_left_1= 0, hip_left_1_to_hip_left_2= 0, hip_left_2_to_thigh_left= 0, thigh_left_to_leg_left= 0, leg_left_to_ankle_left= 0, ankle_left_to_foot_left= 0;

    // message declarations
    sensor_msgs::JointState joint_state;

    while (ros::ok()) {
        //update joint_state
        joint_state.header.stamp = ros::Time::now();
        joint_state.name.resize(12);
        joint_state.position.resize(12);

        joint_state.name[0] ="base_to_hip_right_1";
        joint_state.position[0] = base_to_hip_right_1;
        joint_state.name[1] ="hip_right_1_to_hip_right_2";
        joint_state.position[1] = hip_right_1_to_hip_right_2;
        joint_state.name[2] ="hip_right_2_to_thigh_right";
        joint_state.position[2] = hip_right_2_to_thigh_right;
        joint_state.name[3] ="thigh_right_to_leg_right";
        joint_state.position[3] = thigh_right_to_leg_right;
	joint_state.name[4] ="leg_right_to_ankle_right";
        joint_state.position[4] = leg_right_to_ankle_right;
	joint_state.name[5] ="ankle_right_to_foot_right";
        joint_state.position[5] = ankle_right_to_foot_right;

        joint_state.name[6] ="base_to_hip_left_1";
        joint_state.position[6] = base_to_hip_left_1;
        joint_state.name[7] ="hip_left_1_to_hip_left_2";
        joint_state.position[7] = hip_left_1_to_hip_left_2;
        joint_state.name[8] ="hip_left_2_to_thigh_left";
        joint_state.position[8] = hip_left_2_to_thigh_left;
        joint_state.name[9] ="thigh_left_to_leg_left";
        joint_state.position[9] = thigh_left_to_leg_left;
        joint_state.name[10] ="leg_left_to_ankle_left";
        joint_state.position[10] = leg_left_to_ankle_left;
        joint_state.name[11] ="ankle_left_to_foot_left";
        joint_state.position[11] = ankle_left_to_foot_left;

        //send the joint state and transform
        joint_pub.publish(joint_state);

        // Code that mave robot "move"
	if (hip_right_2_to_thigh_right >= 1.5708){
		wise = -1;}
        if (hip_right_2_to_thigh_right <= 0){
                wise = 1;}
        hip_right_2_to_thigh_right = hip_right_2_to_thigh_right + wise*diferential;
        thigh_right_to_leg_right = thigh_right_to_leg_right - wise*diferential*0.8;

        // This will adjust as needed per iteration
        loop_rate.sleep();
    }


    return 0;
}
